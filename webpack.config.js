const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
    entry: {
        app: './src/entry-points/app.js',
    },
    output: {
        filename: './dist/js/[name].min.js',
        path: path.resolve(__dirname)
    },
    devtool: 'inline-source-map',
    devServer: {
        stats: "errors-only",
        host: process.env.HOST,
        port: process.env.PORT,
        open: true,
        overlay: true,
    },
    module: {
      rules: [
          { //Eslint
              enforce: "pre",
              test: /\.js$/,
              exclude: /node_modules/,
              loader: "eslint-loader"
          },
          { //Babel
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader",
                options: {
                    presets: ['@babel/preset-env'],
                    plugins: ['@babel/plugin-proposal-class-properties']
                }
            }
          },
          { //Css
              test: /\.css$/,
              use: ['style-loader', 'css-loader']
          },
          { //Scss
              test: /\.(sass|scss)$/,
              use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
          },
          { //Images
              test: /\.(png|svg|jpg|gif)$/,
              use: [
                  {
                      loader: 'file-loader',
                      options: {
                          outputPath: 'dist/img',
                          publicPath: ''
                      }
                  }
              ]
          },
          { //Html
              test: /\.html$/,
              loader: "raw-loader"
          }
      ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: './dist/css/app.min.css'
        })
    ],
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
            cache: true,
            parallel: true
            }),
            new OptimizeCSSAssetsPlugin({})
        ],
    },
};