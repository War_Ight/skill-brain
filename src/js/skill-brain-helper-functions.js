import { MainSkill, Skill } from './skill';

/**
 * Parse HTML element and return Skill object
 * @param element - Element which to get skill from
 * @return {Skill} Skill
 */
function getSkillFromElement(element)
{
  const name = element.getElementsByTagName('h1')[0].innerHTML;
  const description = element.getElementsByTagName('p')[0].innerHTML;
  const weight = parseFloat(element.dataset.weight);
  if (Number.isNaN(weight))
  {
    throw new TypeError('weight cannot be converted to number');
  }

  return new Skill(name, description, weight);
}

/**
 * Parse a html object and return a set of skills and sub skills from it in json
 * @description Creates a SkillBrain from a canvas id and optionally a Skill array.
 * If the Skill array is supplied, one will try to be created from the inner html of the canvas
 * It will look for the following in each HTML LI element within a HTML ul element:
 * - 1 nested HTML div with the class 'main-skill'
 * - Multiple nested HTML divs with the class 'sub-skill'
 *
 * Both the 'main-skill' div and 'sub-skill' div are Skill divs
 * A Skill HTML div requires the following:
 * - 1 HTML H1 heading element: This will be the name of the Skill
 * - 1 HTML P paragraph element: This will be the description of the Skill
 * @param containerId
 * @return {Skill[]} Skills obtained from HTML
 */
function GetSkillsFromHtml(containerId)
{
  const container = document.getElementById(containerId).getElementsByTagName('ul')[0];
  if (!container)
  {
    throw new Error('Ul element not found within container');
  }

  const listItems = container.getElementsByTagName('li');
  if (listItems.length < 1)
  {
    throw new Error('No li elements found within ul element');
  }

  const mainSkills = [];
  for (let i = 0; i < listItems.length; i += 1)
  {
    const mainSkillElement = listItems[i].getElementsByClassName('main-skill')[0];
    const subSkillElements = listItems[i].getElementsByClassName('sub-skill');

    if (!mainSkillElement)
    {
      throw new Error('Element with class "main-skill" not found in li');
    }

    if (subSkillElements.length < 1)
    {
      throw new Error('No Elements with class "sub-skill" not found in li');
    }

    const subSkills = [];
    for (let j = 0; j < subSkillElements.length; j += 1)
    {
      const subSkill = getSkillFromElement(subSkillElements[j]);
      subSkills.push(subSkill);
    }

    const mainSkill = new MainSkill(getSkillFromElement(mainSkillElement), subSkills);
    mainSkills.push(mainSkill);
  }

  return mainSkills;
}
export default GetSkillsFromHtml;
