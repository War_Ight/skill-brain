import GetSkillsFromHtml from './skill-brain-helper-functions';

/**
 * @summary Main Class for SkillBrain Library
 * @description The SkillBrain class is the main class for the Skill-Brain library.
 * It needs an id of a canvas and a set of skills to create a visual SkillBrain from.
 */
class SkillBrain
{
  /**
   * The constructor of the SkillBrain class
   * @param {string} canvasId - Id of the canvas
   * @param {Skill[]} skills - List of main skills and corresponding sub skills
   */
  constructor(canvasId, skills)
  {
    this.skills = skills;

    this.Render();
  }
  
  /**
   * @summary Creates a SkillBrain from a canvas id and optionally a Skill array
   * @see #GetSkillsFromHtml
   * @param {string} canvasId - Id of targeted canvas
   * @param {Skill[]} [skills] - Optional skill array
   * @return {SkillBrain} SkillBrain created from canvas and corresponding skills
   */
  static Create(canvasId, skills) // In future allow to pass in skills through javascript
  {
    if (!skills)
    {
      return new SkillBrain(canvasId, GetSkillsFromHtml(canvasId));
    }

    return new SkillBrain(canvasId, skills);
  }

  Render()
  {
    console.log(this);
  }
}
export default SkillBrain;
