/** Generic Skill class */
export class Skill
{
  /**
   * The constructor of the Skill class
   * @param {string} name - The name of the skill
   * @param {string} description - The description of the skill
   * @param {number} weight - The weight of the skill
   * @return {Skill} Skill
   */
  constructor(name, description, weight)
  {
    this.name = name;
    this.description = description;
    this.weight = weight;
  }
}

/** Extension of the Skill class, it can take in SubSkills */
export class MainSkill extends Skill
{
  /**
   *  The constructor of the MainSkill class
   * @param {Skill} mainSkill - Main Skill
   * @param {Skill[]} subSkills - Array of Skills used as 'sub skills'
   */
  constructor(mainSkill, subSkills)
  {
    super(mainSkill.name, mainSkill.description, mainSkill.weight);
    this.subSkills = subSkills;
  }
}
