import paper, {
  Path, Rectangle, Point, Color,
} from 'paper';

/* Math & Manipulation Functions */
function getDistanceBetweenTwoPoints(point1, point2)
{
  return Math.sqrt(((point2.x - point1.x) ** 2) + ((point2.y - point1.y) ** 2));
}

function getPointInLine(distanceInLine, linePoints, lineDistance)
{
  const x = linePoints[0].x + (distanceInLine / lineDistance) * (linePoints[1].x - linePoints[0].x);
  const y = linePoints[0].y + (distanceInLine / lineDistance) * (linePoints[1].y - linePoints[0].y);
  return new Point(x, y);
}

function getPointsWhereLineIntersectsPath(linePoints, path)
{
  let firstIntersection;
  let lastIntersection;
  let lastPointWasInPath;

  const totalLineDistance = getDistanceBetweenTwoPoints(linePoints[0], linePoints[1]);

  let lastPoint = getPointInLine(0, linePoints, totalLineDistance);
  for (let i = 0; i < totalLineDistance; i += 1)
  {
    const currentPoint = getPointInLine(i, linePoints, totalLineDistance);

    const pointNoLongerInPath = lastPointWasInPath && !path.contains(currentPoint);
    const pointNoLongerOutsidePath = !lastPointWasInPath && path.contains(currentPoint);
    if (pointNoLongerInPath || pointNoLongerOutsidePath)
    {
      if (!firstIntersection)
      {
        firstIntersection = lastPoint;
      }
      else if (firstIntersection && !lastIntersection)
      {
        lastIntersection = lastPoint;
        return [firstIntersection, lastIntersection];
      }
    }

    lastPointWasInPath = path.contains(currentPoint);
    lastPoint = currentPoint;
  }

  return null;
}

function getEndOfLinePointFromOriginFromAngle(angle, length)
{
  const theta = angle * Math.PI / 180;
  const dx = (length / 2) * Math.cos(theta);
  const dy = (length / 2) * Math.sin(theta);

  return new Point(dx, dy);
}

function getEndsOfLineThroughPointAtAngle(point, angle, length)
{
  let startOfLinePointMaxAngle = getEndOfLinePointFromOriginFromAngle(angle, -length);
  startOfLinePointMaxAngle = point.subtract(startOfLinePointMaxAngle);

  let endOfLinePointMaxAngle = getEndOfLinePointFromOriginFromAngle(angle, length);
  endOfLinePointMaxAngle = point.subtract(endOfLinePointMaxAngle);

  return [startOfLinePointMaxAngle, endOfLinePointMaxAngle];
}

/* Drawing Functions */
function drawCircleAtPoint(point, color, opacity = 0.5, size = 20)
{
  const circle = new Path.Circle(point, size);
  circle.fillColor = color;
  circle.fillColor.alpha = opacity;
}

function drawCirclesAtPoints(points, color, opacity = 0.5, size = 20)
{
  for (let i = 0; i < points.length; i += 1)
  {
    drawCircleAtPoint(points[i], color, opacity, size);
  }
}

/* Other Functions */
function pointToPathFriendlyString(point)
{
  return `${point.x} ${point.y}`;
}

function p2s(point)
{
  return pointToPathFriendlyString(point);
}


window.onload = () =>
{
  // Reference Canvas
  paper.setup('skill-brain');

  // Create background
  const background = new Path.Rectangle(new Rectangle(0, 0, 1000, 1000));
  background.fillColor = '#e8e8e8';

  // Center Point
  const centerPoint = new Point(500, 500);
  const centerPointRadius = 10;

  // Create Compartment
  const compartment = new Path('M 25 575 Q 325 600 450 450 Q 600 200 525 75 Q 425 0 100 100 Q 0 325 25 575 Z');
  compartment.position = centerPoint;
  compartment.fillColor = 'grey';
  compartment.strokeColor = 'black';
  compartment.strokeWidth = 2;

  // Center Radius Circle
  const centerRadiusCircle = new Path.Circle(centerPoint, centerPointRadius * 2);
  centerRadiusCircle.fillColor = new Color(255, 0, 0);
  centerRadiusCircle.fillColor.alpha = 0.5;

  // Angle constrains
  const maxAngle = 20;
  const minAngle = -20;

  const sweetSpotAngles = [minAngle, 5, maxAngle];

  const sweetSpotsFirst = [];
  const sweetSpotsLast = [];

  for (let i = 0; i < sweetSpotAngles.length; i += 1)
  {
    const linePoints = getEndsOfLineThroughPointAtAngle(centerPoint, sweetSpotAngles[i], 600);

    // Draw Line
    const linePath = new Path.Line(linePoints[0], linePoints[1]);
    linePath.strokeWidth = 3;
    linePath.strokeColor = 'blue';
    linePath.strokeColor.alpha = 0.25;

    // Get sweet points
    const linePathIntersections = getPointsWhereLineIntersectsPath(linePoints, compartment);

    // Draw sweet spots
    sweetSpotsFirst.push(linePathIntersections[0]);
    sweetSpotsLast.push(linePathIntersections[1]);
  }

  sweetSpotsLast.reverse();

  // Draw sweet Spots
  // Red = First | Green = Last
  drawCirclesAtPoints(sweetSpotsFirst, new Color(255, 0, 0));
  drawCirclesAtPoints(sweetSpotsLast, new Color(0, 255, 0));


  //  M 0 0        C 0 0 0 0        100 0
  // First Point | Bezier Handle | Second Point

  // THE INDEX DISTANCE BETWEEN THE 2 POINTS DETERMINES WHAT SIDE GETS A LARGER AREA
  // Use skill weight to determine the size of the index distance between points
  const firstSweetSpot = sweetSpotsFirst[2];
  const lastSweetSpot = sweetSpotsLast[0];


  drawCircleAtPoint(firstSweetSpot, new Color(255, 255, 255), 1);
  drawCircleAtPoint(lastSweetSpot, new Color(255, 255, 255), 1);

  // Use the distance between the index of points to determine the extremeness of the curve
  const firstHandleModifier = (firstSweetSpot.y - centerPoint.y);
  const lastHandleModifier = (lastSweetSpot.y - centerPoint.y);

  const firstSweetSpotHandle = new Point(centerPoint.x, firstSweetSpot.y + firstHandleModifier);
  const lastSweetSpotHandle = new Point(centerPoint.x, lastSweetSpot.y + lastHandleModifier);


  drawCircleAtPoint(firstSweetSpotHandle, new Color(0, 0, 255), 1, 10);
  drawCircleAtPoint(lastSweetSpotHandle, new Color(0, 255, 0), 1, 10);

  const firstSectionPathData = `M ${p2s(firstSweetSpot)} C ${p2s(firstSweetSpotHandle)} ${p2s(lastSweetSpotHandle)} ${p2s(lastSweetSpot)}`;
  const firstSectionPath = new Path(firstSectionPathData);
  firstSectionPath.strokeColor = 'black';
  firstSectionPath.strokeWidth = 5;


  paper.view.draw();
};

/* Animation
 paper.view.onFrame = (event) =>
 {

 let vector = point.subtract(pointOnLine.position);
 vector = vector.divide(50);

 pointOnLine.position = pointOnLine.position.add(vector);

 if (vector.length < 0.25)
 {
 iterativeUnit = randomBetweenTwoNumbers(0, 600) / maxAngleLineDistance;


 const x = maxAngleLinePoints[0].x + iterativeUnit *
 (maxAngleLinePoints[1].x - maxAngleLinePoints[0].x);

 const y = maxAngleLinePoints[0].y + iterativeUnit *
 (maxAngleLinePoints[1].y - maxAngleLinePoints[0].y);

 point = new Point(x, y);
 }

 };
 */
